import json
import os

import cv2
import mediapipe as mp
import argparse

mp_drawing = mp.solutions.drawing_utils
mp_hands = mp.solutions.hands


def predict_folder(img_dir, save_dir):
    """
    Applies mediapipe hand model to the images from img_dir, saves visualizations, and predictions as json file
    in save_dir
    Args:
        img_dir: path to directory containing images only
        save_dir: path to save visualizations, and predictions as json file
    """
    base_dir = os.path.split(img_dir)[0]
    img_list = sorted(os.listdir(img_dir))
    img_names =[]
    for img_name in img_list:
        img_name = os.path.join(base_dir,img_name)
        img_names.append(img_name)

    with mp_hands.Hands(
        static_image_mode=True,
        max_num_hands=2,
        min_detection_confidence=0.4) as hands:

      pred_list = []
      for img_name in img_names:
        # Read an image, flip it around y-axis for correct handedness output (see
        # above).
        image = cv2.flip(cv2.imread(img_name), 1)
        # Convert the BGR image to RGB before processing.
        results = hands.process(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

        # Print handedness and draw hand landmarks on the image.
        print('Handedness:', results.multi_handedness)
        if not results.multi_hand_landmarks:
            print("oops, nothing detected")
            xyz = []
            for i in range(21):
                x, y, z = 0, 0, 0
                xyz.append([x, y, z])
            pred_list.append(xyz)
            cv2.imwrite(os.path.join(save_dir, os.path.split(img_name)[1]), cv2.flip(image, 1))
            continue
        image_height, image_width, _ = image.shape
        annotated_image = image.copy()
        for hand_landmarks in results.multi_hand_landmarks:
          # print('hand_landmarks:', hand_landmarks.landmark[0].x)
          print(
              f'Index finger tip coordinates: (',
              f'{hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].x * image_width}, '
              f'{hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP].y * image_height})'
          )
          mp_drawing.draw_landmarks(
              annotated_image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
        hand_landmark = results.multi_hand_landmarks[0]
        xyz =[]
        for hl in hand_landmark.landmark:
            x,y,z = hl.x, hl.y, hl.z
            xyz.append([x,y,z])
        pred_list.append(xyz)
        cv2.imwrite(os.path.join(save_dir, os.path.split(img_name)[1]), cv2.flip(annotated_image, 1))
    pred_dict = {"preds": pred_list, "img_names": img_list}
    with open(os.path.join(save_dir, "preds.json"), "w") as out:
        json.dump(pred_dict, out, indent=4)


if __name__ == "__main__":
    # predict_folder("/home/user/tmp/FreiHAND/some_imgs/", "/home/user/tmp/FreiHAND/results/")

    parser=argparse.ArgumentParser()
    parser.add_argument("img_dir")
    parser.add_argument("save_dir")
    args = parser.parse_args()
    predict_folder(args.img_dir, args.save_dir)
