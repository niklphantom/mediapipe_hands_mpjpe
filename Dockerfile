FROM python:3.6-slim-stretch

RUN apt-get update ##[edited]
RUN apt-get install ffmpeg libsm6 libxext6  -y

ADD requirements.txt /
RUN pip install -r /requirements.txt

ADD . /app
WORKDIR /app

EXPOSE 5000
CMD [ "python" , "predict_images.py", "/data/in_imgs/", "/data/out_imgs/"]
